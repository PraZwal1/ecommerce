from django.apps import AppConfig


class ContentRecomendationConfig(AppConfig):
    name = 'content_recomendation'
